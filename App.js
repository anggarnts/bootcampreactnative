
import React from "react";
import {View, Text} from 'react-native'

export default class BiodataView extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      name: "Angga",
      address: "Nowhere",
      age: 20,
      hobby: "Music",
      role: "No Role"
    }
  }

  render() {
    return (
      <View flexDirection="row">
        <View backgroundColor="orange" width="50%" flexDirection="column">
          <Text>Name: </Text>
          <Text>Address: </Text>
          <Text>Age: </Text>
          <Text>Hobby: </Text>
          <Text>Role: </Text>
        </View>
        <View backgroundColor="pink" width="50%" flexDirection="column">
          <Text>{this.state.name}</Text>
          <Text>{this.state.address}</Text>
          <Text>{this.state.age}</Text>
          <Text>{this.state.hobby}</Text>
          <Text>{this.state.role}</Text>
        </View>
      </View>
    )
  }
}